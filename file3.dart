/* Create a class and 
 a) then use an object to print 
 the name of the app, sector/category, developer, and the year 
 it won MTN Business App of the Year Awards. 
 b) Create a function inside the class, transform the app name 
 to all capital letters and then print the output.*/

void main () {

    var winningapp2012 = WinningApp();

    winningapp2012.name = "FNB Banking";
    winningapp2012.sector = "Best Consumer Solution";
    winningapp2012.developer = "Developed by First Rand Bank Limited";
    winningapp2012.winningyear = 2012;

    var winningapp2013 = WinningApp();

    winningapp2013.name = "SnapScan";
    winningapp2013.sector = "Best Enetrprise Solution";
    winningapp2013.developer = "Developed by FireID";
    winningapp2013.winningyear = 2013;

    var winningapp2014 = WinningApp();

    winningapp2014.name = "Liveinspect";
    winningapp2014.sector = "Best African Solution";
    winningapp2014.developer = "Developed by Lightstone Auto";
    winningapp2014.winningyear = 2014;

    var winningapp2015 = WinningApp();

    winningapp2015.name = "WumDrop";
    winningapp2015.sector = "Most Innovative Solution";
    winningapp2015.developer = "Developed by WumDrop";
    winningapp2015.winningyear = 2015;

    var winningapp2016 = WinningApp();

    winningapp2016.name = "Domestly";
    winningapp2016.sector = "Best Consumer App";
    winningapp2016.developer = "Developed by Now Boarding";
    winningapp2016.winningyear = 2016;

    var winningapp2017 = WinningApp();

    winningapp2017.name = "Shyft";
    winningapp2017.sector = "Best Financial Solution";
    winningapp2017.developer = "Developed by Standard Bank Group";
    winningapp2017.winningyear = 2017;

    var winningapp2018 = WinningApp();

    winningapp2018.name = "Khula Ecosystem";
    winningapp2018.sector = "Best Agricultural Solution";
    winningapp2018.developer = "Developed by Khula App";
    winningapp2018.winningyear = 2018;

    var winningapp2019 = WinningApp();

    winningapp2019.name = "Naked Insurance";
    winningapp2019.sector = "Best Financial Solution";
    winningapp2019.developer = "Developed by Naked Insurance";
    winningapp2019.winningyear = 2019;

    var winningapp2020 = WinningApp();

    winningapp2020.name = "Easy Equitiies";
    winningapp2020.sector = "Best Financial Solution";
    winningapp2020.developer = "Developed by GT247.com";
    winningapp2020.winningyear = 2020;

    var winningapp2021 = WinningApp();

    winningapp2021.name = "Ambani Africa";
    winningapp2021.sector = "Best App of the Year";
    winningapp2021.developer = "Developed by Ambani Africa";
    winningapp2021.winningyear = 2021;

    print("Below is a list of the MTN Business App of the Year Winning Apps:");
    
    winningapp2012.printWinningAppdetails();
    winningapp2013.printWinningAppdetails();
    winningapp2014.printWinningAppdetails();
    winningapp2015.printWinningAppdetails();
    winningapp2016.printWinningAppdetails();
    winningapp2017.printWinningAppdetails();
    winningapp2018.printWinningAppdetails();
    winningapp2019.printWinningAppdetails();
    winningapp2020.printWinningAppdetails();
    winningapp2021.printWinningAppdetails();
  
}

class WinningApp {

  String? name;
  String? sector;
  String? developer;
  int? winningyear;

  void printWinningAppdetails() {
  
    print(name);
    print(sector);
    print(developer);
    print(winningyear);

    }
  

}

 

